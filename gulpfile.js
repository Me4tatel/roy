var gulp = require('gulp'),
	less = require('gulp-less'),
	browserSync = require('browser-sync'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglifyjs'),
	cssnano = require('gulp-cssnano'),
	rename = require('gulp-rename'),
	del = require('del'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	cache = require('gulp-cache'),
	autoprefixer = require('gulp-autoprefixer'),
	cssimport = require("gulp-cssimport"),
	options = {};

const { series, parallel } = require('gulp');

const lessTask = function(done) {
	gulp.src('./app/css/**/*.less')
		.pipe(less())
		.pipe(cssimport(options))
		.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {
			cascade: true
		}))
		.pipe(gulp.dest('./app/css'))
		.pipe(cssnano())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest('./app/css'))
		.pipe(browserSync.stream({match: '**/*.css'}));
	done();
};

const BrowserSync = function(done) {
	browserSync({
		server: {
			baseDir: './app'
		},
		notify: false
	});
	done();
};

const scripts = function(done) {
	gulp.src([
			'app/libs/select2/dist/js/select2.min.js',
			'app/libs/semantic/semantic.min.js',
			'app/libs/zebra_datepicker/dist/zebra_datepicker.min.js',
			'app/libs/inputmask/dist/jquery.inputmask.bundle.js',
			'app/libs/iban/iban.js'
			
		])
		.pipe(concat('libs.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./app/js'));
	done();
};

// const importCSS = function(done) {
// 	gulp.src("./app/css/libs.less")
// 		.pipe(less())
// 		.pipe(gulp.dest("app/css"));
// 	done();
// };

// const cssLibs = series(lessTask, function(done) {
// 	gulp.src('./app/css/libs.css')
// 		.pipe(cssnano())
// 		.pipe(rename({
// 			suffix: '.min'
// 		}))
// 		.pipe(gulp.dest('app/css'));
// 	done();
// });

const reload = function(done) {
  browserSync.reload();
  done();
}

const stream = function(done) {
  browserSync.stream();
  done();
}

const watch = series(BrowserSync, scripts, function(done) {
	gulp.watch('./app/css/**/*.less', gulp.series(lessTask));
	gulp.watch('./app/css/**/*.less', reload);
	gulp.watch('./app/*.html', reload);
	gulp.watch('./app/**/*.js', reload);
});

const clean = function(done) {
	del.sync('dist');
	done();
};

const img = function(done) {
	gulp.src('./app/img/**/*')
		.pipe(cache(imagemin({
			interlaced: true,
			progressive: true,
			svgoPlugins: [{
				removeViewBox: false
			}],
			use: [pngquant()]
		})))
		.pipe(gulp.dest('./dist/img'));
	done();
};

const build = series(clean, img, lessTask, scripts, function(done) {

	var buildCss = gulp.src([ // all css
			'./app/css/main.css',
			'./app/css/main.min.css',
			'./app/css/libs.min.css',
			'./app/css/fonts.css'
		])
		.pipe(gulp.dest('./dist/css'))

	var buildFonts = gulp.src('./app/fonts/**/*') // all fonts
		.pipe(gulp.dest('./dist/fonts'))

	var buildAssets = gulp.src('./app/assets/**/*') // assets
		.pipe(gulp.dest('./dist/assets'))

	var buildJs = gulp.src('./app/js/**/*') // all js
		.pipe(gulp.dest('./dist/js'))

	var buildHtml = gulp.src('./app/*.html') //all html
		.pipe(gulp.dest('./dist'));
	done();
});


const defaultTask = series(watch);

exports.watch = watch;

exports.build = build;

exports.default = defaultTask;