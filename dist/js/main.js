'use strict'
$('.bullets .bullet').on('click', function setActiveBullet() {
    $(this)
        .addClass('active')
        .siblings()
        .removeClass('active');
    $(this)
        .closest('.main-block')
        .find('.content[data-id="' + $(this).data('tab-id') + '"]')
        .addClass('active')
        .siblings()
        .removeClass('active');
    setActiveContent();
});

function setActiveContent() {
    var mainBlock = $('.main-block');
    var activeContent = mainBlock.find('.content.active .innner-content');
    var bullets = mainBlock.find('.bullets');
    var heightActiveContent = activeContent.height();
    var heightBullets = bullets.outerHeight();
    var widthActiveContent = activeContent.width();
    $('.main-block').height(heightActiveContent + heightBullets);
    $('.main-block').width(widthActiveContent);
}


if ($('.main-select').length) {
    var mainHtml = '<option></option>';
    json.forEach(function(item) {
        mainHtml += '<option value="' + item.value + '" ' + (item.selected ? "selected" : '') + '>' + item.title + '</option>';
    });
    var attr = $('.main-select').attr('multiple');
    if (typeof attr !== typeof undefined && attr !== false) {
        mainHtml += '<option value="all">Selecteer alles</option>';
    }
    $('.main-select').html(mainHtml);
    $('.main-select').on('change', function() {
        var html = '<option></option>';
        var _this = $(this);
        let currentItems = json.filter(function(item) {
            if(Array.isArray(_this.val())){
                if (_this.val().indexOf(item.value) !== -1) {
                    return true;
                }
            }else{
                if (_this.val() === item.value) {
                    return true;
                }
            }
            return false;
        });
        var value = $(this).closest('.form').find('.sub-select').val();
        var lengthSubItems = 0;
        if(Array.isArray(value)){
            if (currentItems.length) {
                currentItems.forEach(function(parent){
                    parent.items.forEach(function(item) {
                        lengthSubItems++;
                        html += '<option value="' + item.value + '" ' + (item.selected || value.indexOf(item.value) !== -1 ? "selected" : '') + '>' + item.title + '</option>';
                    });
                })
            }
        } else {
            if (currentItems.length) {
                currentItems.forEach(function(parent){
                    parent.items.forEach(function(item) {
                        lengthSubItems++;
                        html += '<option value="' + item.value + '" ' + (item.selected || value === item.value ? "selected" : '') + '>' + item.title + '</option>';
                    });
                })
            }
        }
        if(lengthSubItems){
            var attr = $(this).closest('.form').find('.sub-select').attr('multiple');
            if (typeof attr !== typeof undefined && attr !== false) {
                html += '<option value="all">Select All</option>';
            }
            
        }
        $(this).closest('.form').find('.sub-select').html(html);
    });
    $('.main-select').trigger('change');
}

setActiveContent();
var datepickers = [];
var value, select_name, optgroup_label;
window.onload = function() {
    $('.main-block').addClass('loaded');
    $(".select-without-search").select2({
        minimumResultsForSearch: -1
    })
    $(".select-with-search").select2({
        minimumResultsForSearch: -1
    }).on('change', setActiveContent);
    $('input.datepicker').each(function() {
        $(this).Zebra_DatePicker({
            format: 'Y-m-d',
            default_position: "below",
            readonly_element: false,
            enabled_minutes: [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55]
        })
    });
    setActiveContent();
    $(window).trigger('resize');
    if($('.filters').length){
        checkFilters(true);
    }
}

$(window).on('resize', function() {
    setActiveContent();
    if ($(window).width() <= 767) {
        $('input.datepicker').each(function() {
            $(this).data('Zebra_DatePicker').update({
                default_position: "above"
            });
        })
    } else {
        $('input.datepicker').each(function() {
            $(this).data('Zebra_DatePicker').update({
                default_position: "below"
            });
        })
    }
});

function handleFiles() {
    var fileList = this.files;
}

$(document).on('click', '.list-files .remove-file', function() {
    $(this).closest('li').remove();
});

$('.add-photo').each(function() {
    var mainWrapper = $(this);
    $(this).find('.input-file').on('change', function(e) {
        $(this).closest('.field').removeClass('error');
        var allowedExtensions = /(\.jpg|\.jpeg|\.svg|\.png|\.gif)$/i;
        var files = $(this).get(0).files;
        var htmlList = '';
        for (var i = 0; i < files.length; i++) {
        	console.log(files[i].size);
            if(allowedExtensions.exec(files[i].name) && files[i].size <= parseInt(mainWrapper.data('max-size'), 10)){
                htmlList += '<li>' + files[i].name + '<span class="remove-file"><span></li>';
            }else{
            	if(!allowedExtensions.exec(files[i].name)){
        			$(this).closest('.field').find('.format').removeClass('hidden').siblings().addClass('hidden');
            	}else{
        			$(this).closest('.field').find('.size').removeClass('hidden').siblings().addClass('hidden');
            	}
                $(this).closest('.field').addClass('error');
            }
        }
        mainWrapper.find('.list-files').html(htmlList);
        setActiveContent();
    });
});

function getChar(event) {
    if (event.which == null) {
        if (event.keyCode < 32) return null;
        return String.fromCharCode(event.keyCode)
    }
    if (event.which != 0 && event.charCode != 0) {
        if (event.which < 32) return null;
        return String.fromCharCode(event.which)
    }
    return null;
}

$('[name="phone"], [name="card_number"], [name="card_cvv"], [name="card_date"]').on('keypress paste', function(e) {
    e = e || event;
    if (e.ctrlKey || e.altKey || e.metaKey) return;
    var chr = getChar(e);
    if (chr == null) return;
    if ((chr == '.' || chr == '/' || chr == '-' || chr == '(' || chr == ')' || chr == '+' || chr == ' ') && ($(this).attr('name') == "phone" || $(this).attr('type') == "tel")) return;
    if (chr < '0' || chr > '9') return false;
});

$('.next-step').on('click', validate);
$('.prev-step').on('click', function(){
    $(this).closest('.content').prev().addClass('active')
            .siblings()
            .removeClass('active');
        $(this)
            .closest('.main-block')
            .find('.bullet[data-tab-id="' + $(this).closest('.content')
                .prev().data('id') + '"]')
            .addClass('active')
            .siblings()
            .removeClass('active');
        setActiveContent();
});
function validate(e){
    var elements = $(this).closest('form').get(0).elements;
    var error = false;
    var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var phoneRegex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
    for (var i = 0; i < elements.length; i++) {
        if (elements[i].required) {
            var el = $(elements[i]);
            el.closest('.field').removeClass('error');
            if (!elements[i].validity.valid) {
                el.closest('.field').addClass('error')
                error = true;
            }
            if (el.attr('name') === 'password_repeat') {
                if (el.val() !== $('input[name="password"]').val()) {
                    el.closest('.field').addClass('error');
                    error = true;
                }
            }

            if (el.attr('type') === 'email') {
                if (!emailRegex.test(el.val())) {
                    el.closest('.field').addClass('error');
                    error = true;
                }
            }
            if (el.attr('type') === 'tel') {
                if (!phoneRegex.test(el.val().split(' ').join('')) || !Inputmask.isValid(el.val(), { inputFormat: '+99 (999) 99 99 999'}) || el.val().split(' ').join('').length!== 10) {
                    el.closest('.field').addClass('error');
                    error = true;
                }
            }
            if (el.attr('multiple')) {
                if (!el.val() || !el.val().length) {
                    el.closest('.field').addClass('error');
                    error = true;
                }
            }
        }
    }
    if (!error) {
        $(this).closest('.content').next().addClass('active')
            .siblings()
            .removeClass('active');
        $(this)
            .closest('.main-block')
            .find('.bullet[data-tab-id="' + $(this).closest('.content')
                .next().data('id') + '"]')
            .addClass('active')
            .siblings()
            .removeClass('active');
        setActiveContent();
    }
    if (error){
        e.preventDefault();
        return false;  
    }
    return true
}

$('[name="card_date"]').inputmask('99/9999');

$('.ui.sticky')
    .sticky({
        context: '.wrapper-for-stiky'
    });
$('.tabs .item')
    .tab();

$('.create-order, .update-data').on('click', function(e) {
    if(!validate.call(this, e))return;
    var _this = $(this);
    e.preventDefault();
    var data = new FormData();
    data.append('action', 'create_order');
    data.append('first_name', $("input[name=first_name]").val());
    data.append('last_name', $("input[name=last_name]").val());
    data.append('phone', $("input[name=phone]").val());
    data.append('email', $("input[name=email]").val());
    data.append('industry', $('select[name="industry"]').val());
    data.append('services', $('select[name="services"]').val());
    data.append('regions', $('select[name="regions"]').val());
    data.append('building_address', $("input[name=building_address]").val());
    data.append('notes', $("input[name=notes]").val());
    data.append('description', $('input[name=description]').val());
    data.append('estimate', $('input[name=time-needs-finished]').val());
    data.append('urgency', $('select[name="urgency"]').val());
    if($('.input-file').length){
    	var files = $('.input-file').get(0).files;
    	$('.list-files').find('li').each(function(){
    		for(var fileIndex in files){
    			if(files[fileIndex].name === $(this).text()){
    				data.append('photos[]', files[fileIndex]);
    			}
    		}
    	})
    }
    $.post(ajax_url, data, function(response) {
        if (response === "complete") {
            _this.closest('.content').next().addClass('active')
                .siblings()
                .removeClass('active');
            _this
                .closest('.main-block')
                .find('.bullet[data-tab-id="' + $(this).closest('.content')
                    .next().data('id') + '"]')
                .addClass('active')
                .siblings()
                .removeClass('active');
            setActiveContent();
        }
    });
});
function alertValidIBAN(iban) {
    alert(isValidIBANNumber(iban));
}
/*
 * Returns 1 if the IBAN is valid 
 * Returns FALSE if the IBAN's length is not as should be (for CY the IBAN Should be 28 chars long starting with CY )
 * Returns any other number (checksum) when the IBAN is invalid (check digits do not match)
 */

$('.check-form').on('click', function(){
    var content = $(this).closest('.inner-sticky-content');
    var error = false;
    content.find('.field').removeClass('error');
    if(!IBAN.isValid((content.find('[name="iban-code"]').val()))){
        error = true;
        content.find('[name="iban-code"]').closest('.field').addClass('error');
    }
    if(!content.find('[name="holder-name"]').val()){
        error = true
        content.find('[name="holder-name"]').closest('.field').addClass('error');
    }
    if(!content.find('[name="nrc"]').is(':checked')){
        error = true
        content.find('[name="nrc"]').closest('.field').addClass('error');
    }
    if(!content.find('[name="akkoord"]').is(':checked')){
        error = true
        content.find('[name="akkoord"]').closest('.field').addClass('error');
    }
    if(!error){
        content.removeClass('active');
        var idContent = content.data('content') + 1;
        content.siblings('[data-content="' + idContent + '"]').addClass('active');
    }
});
$('select[multiple]').on('change', function(){
    if($(this).val() && $(this).val().indexOf("all") !== -1){
        var selected = [];
        $(this).find('option:not([value="all"])').each(function(i,e){
            if($(this).attr("value")){
                selected[selected.length] = $(this).attr("value");
            }
        });
        $(this).val(selected);
        $(this).trigger('change');
    }
});

if($('.filters').length){
    var dateInput = $('[name="date"]');
    var industryInput = $('[name="industry"]');
    var locationInput = $('[name="location"]');
    dateInput.on('change', checkFilters);
    industryInput.on('change', checkFilters);
    locationInput.on('change', checkFilters);
    $('.btn-reset').on('click', function(){
        dateInput.val('');
        if(industryInput.is("[multiple]")){
            industryInput.val([]);
        }else{
            industryInput.val('');
        }
        if(locationInput.is("[multiple]")){
            locationInput.val([]);
        }else{
            locationInput.val('');
        }
        dateInput.trigger('change');
        industryInput.trigger('change');
        locationInput.trigger('change');
    });
}

function checkFilters(load){
    if(!dateInput.val() && 
        !(locationInput.val() && (locationInput.is("[multiple]") ? locationInput.val().length : true)) && 
        !(industryInput.val() && (industryInput.is("[multiple]") ? industryInput.val().length : true))){
        $('.btn-reset').addClass('hidden');
    }else{
        $('.btn-reset').removeClass('hidden');
    }
    if(load !== true){
        getHistory();
    }
}

function getHistory(){
    var dateInput = $('[name="date"]');
    var industryInput = $('[name="industry"]');
    var locationInput = $('[name="location"]');

    var industry = '';
    var location = '';
    var date = dateInput.val() || ""

    if(industryInput.is("[multiple]")){
        industry = []
    }
    if(locationInput.is("[multiple]")){
        location = []
    }


    if(industryInput.val()){
        industry = industryInput.val()
    }
    if(locationInput.val()){
        location = locationInput.val()
    }

    // $.ajax({
    //     type:'POST',
    //     url:'',
    //     data:{
    //         date,
    //         industry,
    //         location
    //     },
    //     contentType: 'application/json',
    //     dataType: 'json',
    //     beforeSend: function(){
    //         $('.history-blocks .dimmer').addClass('active');
    //     },
    //     success: function(response){
    //         $('.history-blocks').html(response.history);
    //         $('.pagination').html(response.pagination);
    //         $('.history-blocks .dimmer').removeClass('active');
    //     },
    //     error: function(error){
    //     	$('.modal.error').modal('show').find('p').html(error.statusText);
    //     }
    // })
}

$('.modal .actions .positive').on('click', function(){
    $('.history-blocks .dimmer').removeClass('active');
})

$('.toggle-icon').on('click', function(){
    $(this).closest('.wrap-input').toggleClass('show');
});
$('.password').on('keypress keyup keydown paste copy cut', function(){
    $('.show-password').val($(this).val());
    checkPassword($(this));
});
$('.show-password').on('keypress keyup keydown paste copy cut', function(){
    $('.password').val($(this).val());
    checkPassword($(this));
});

function scorePassword(pass) {
    var score = 0;
    if (!pass)
        return score;

    // award every unique letter until 5 repetitions
    var letters = new Object();
    for (var i=0; i<pass.length; i++) {
        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
        score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up
    var variations = {
        digits: /\d/.test(pass),
        lower: /[a-z]/.test(pass),
        upper: /[A-Z]/.test(pass),
        nonWords: /\W/.test(pass),
    }

    var variationCount = 0;
    for (var check in variations) {
        variationCount += (variations[check] == true) ? 1 : 0;
    }
    score += (variationCount - 1) * 10;

    return parseInt(score);
}

function checkPassStrength(pass) {
    var score = scorePassword(pass);
    if (score > 80)
        return "heavy";
    if (score > 60)
        return "medium";
    if (score >= 30)
        return "easy";
    if (score < 30)
        return "very bad";
    return "";
}

function checkPassword(el){
    var score = scorePassword(el.val());
    console.log(el, score);
    el.closest('.wrap-input').find('.strong').removeClass('heavy medium easy bad')
    if (score > 80){
        el.closest('.wrap-input').find('.strong').addClass('heavy');
    }
    if (score > 60){
        el.closest('.wrap-input').find('.strong').addClass('medium');
    }
    if (score >= 30){
        el.closest('.wrap-input').find('.strong').addClass('easy');
    }
    if (score < 30){
        el.closest('.wrap-input').find('.strong').addClass('bad');
    }
    el.closest('.wrap-input').find('.strong .strong-word').html(checkPassStrength(el.val()));
}

$(document).on('change', '[name="service_looking_for"]', function(e){
    checkDescription();
});
$(document).on('change keypress keyup keydown', '[name="description"]', function(e){
    checkDescription();
});

function checkDescription(){
    var statuses = ["Zwak", "Matig", "Prima", "Goed", "Uitstekend!"];
    var selectedServices = $('[name="service_looking_for"]');
    var description = $('[name="description"]');
    var quality = description.data('quality');
    var comments = description.data('comments');
    var fieldDescription = description.closest('.field');
    var progressBar = fieldDescription.find('.progress');
    var progresStatusField = progressBar.find('.progress-status');
    var descriptionBlock = fieldDescription.find('.description');
    if(selectedServices.val()){
        descriptionBlock.html(comments[selectedServices.val()]);
    }
    var level = 0;
    var lengthDescription = description.val().length;
    console.log(comments, selectedServices.val());
    if(quality[0] && lengthDescription > quality[0]){
        level = 1
    }
    if(quality[1] && lengthDescription > quality[1]){
        level = 2
    }
    if(quality[2] && lengthDescription > quality[2]){
        level = 3
    }
    if(quality[3] && lengthDescription > quality[3]){
        level = 4
    }

    progresStatusField.html(statuses[level], selectedServices.value);
    progressBar.attr('data-progress-count', level);

    setActiveContent();
}