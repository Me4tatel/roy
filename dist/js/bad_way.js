$('[data-bad-way]').on('click', function(e){
    e.preventDefault();
    $('body').addClass('show-bad-way');
    setTimeout(function(){
        $('body').removeClass('show-bad-way');
    }, 5000)
});

$('body').append("<style>body.show-bad-way .modal-bad-way{transform: none;opacity: 1;transition: opacity .3s, transform 0s 0s;}.modal-bad-way{position: fixed;top: 0;left: 0;width: 100vw;height: 100vh;transform: translateX(-110vw);opacity: 0;overflow: hidden;display: -webkit-flex;display: -moz-flex;display: -ms-flex;display: -o-flex;display: flex;transition: opacity .3s, transform 0s .3s;background: rgba(0,0,0, .8);color: #fff;text-align: center;justify-content: center;-ms-align-items: center;align-items: center;font-size: 30px;z-index: 99999999999;}</style><div class='modal-bad-way'><span>Don't worry, be happy :)</span></div>");